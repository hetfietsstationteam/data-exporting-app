﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data_exporting
{
    public enum DiscountType
    {
        [Display(Description = "Geen")]
        None = 0,
        [Display(Description = "Percentage")]
        Percentage = 1,
        [Display(Description = "Coupon")]
        Coupon = 2,
        [Display(Description = "Bedrag")]
        Amount = 3
    }

    public class Sale
    {
        public int Id { get; set; }
        //public DiscountType DiscountType { get; set; }
        //public decimal? DiscountValue { get; set; }
        //public decimal Btw { get; set; }
        public DateTime Sold_At { get; set; }
        public decimal? FinalPrice { get; set; }
        public int BikeId { get; set; }
        public System.Guid Uuid { get; set; }
        //public string Reference { get; set; }
        //public string TransactionId { get; set; }
        //public DateTime? Terminal_Sold_At { get; set; }
    }
}
