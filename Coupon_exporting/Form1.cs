﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data_exporting
{
    public partial class Form1 : Form
    {
        public List<Coupon> coupons = new List<Coupon>();
        public List<Bike> bikes = new List<Bike>();

        public string Cell1 { get; set; }
        public string Cell2 { get; set; }
        public string Cell3 { get; set; }

        public decimal CouponVal { get; set; }

        // public static readonly string host_url = "https://begin.beginstation.nl:444";
        public static readonly string host_url = "http://192.168.1.143";
        public static readonly string host = $"{host_url}/api/v1/";

        string Token = null;

        public Form1()
        {
            InitializeComponent();
            progressBar1.Visible = false;
            Uploadbtn.Enabled = false;
            importbtn.Enabled = false;
            addsingelCouponPanel.Enabled = false;
            SetCouponeValue();
        }

        private void SetCouponeValue()
        {
            if (int.TryParse(CouponValue.Text, out int Results))
                CouponVal = Results;
            else
                CouponVal = 10;
        }

        private void importbtn_Click(object sender, EventArgs e)
        {
            if (BikeButton.Checked)
            {
                ImportBikesFromExcel();
            }

            if (CouponsButton.Checked)
            {
                ImportCouponsFromExcel();
            }
            BikeButton.Enabled = false;
            CouponsButton.Enabled = false;

            if (!BikeButton.Checked && !CouponsButton.Checked)
            {
                BikeButton.Enabled = true;
                CouponsButton.Enabled = true;
                MessageBox.Show("Select an option", "INFO");
            }
        }

        private void CouponValue_TextChanged(object sender, EventArgs e)
        {
            SetCouponeValue();
        }

        public async Task<bool> StoreCoupon(Coupon coupon)
        {
            HttpClient httpClient = new HttpClient();

            var json = JsonConvert.SerializeObject(coupon);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Token", Token);
            var result = await httpClient.PostAsync(host + "coupon/", content);
            if (result.StatusCode == System.Net.HttpStatusCode.Created)
                return true;

            return false;
        }

        private async void Uploadbtn_Click(object sender, EventArgs e)
        {
            Uploadbtn.Enabled = false;
            importbtn.Enabled = false;
            progressBar1.Visible = true;
            richTextBox1.Clear();
            int addedItemsCounter = 0;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            if (BikeButton.Checked)
            {
                progressBar1.Maximum = bikes.Count();
                foreach (var bike in bikes)
                {
                    Bike newBike = await StoreBikes(bike);
                    Sale newSale = null;
                    if (newBike != null)
                    {
                        bike.Id = newBike.Id;

                        if (bike.Sale != null)
                        {
                            bike.Sale.BikeId = bike.Id;
                            newSale = await StoreSales(bike.Sale);
                            if (newSale == null)
                                Console.WriteLine($"bike #{bike.OldId} sale import failed!");
                        }

                        progressBar1.PerformStep();
                        richTextBox1.Text = addedItemsCounter + "  ," + bike.ToString() + "\n";
                        addedItemsCounter++;

                        if (bike.Sale != null)
                            Console.WriteLine($"Imported bike #{bike.OldId} => #{newBike.Id}, sold at {bike.Sale.Sold_At}" );
                        else
                            Console.WriteLine($"Imported bike #{bike.OldId} => #{newBike.Id}");
                    }
                    else
                        Console.WriteLine("Couldn't import" + bike.ToString());
                }

                bikes.Clear();
            }

            if (CouponsButton.Checked)
            {
                progressBar1.Maximum = coupons.Count();

                foreach (var coupon in coupons)
                {
                    if (await StoreCoupon(coupon))
                    {
                        progressBar1.PerformStep();
                        richTextBox1.Text += coupon.ToString() + "\n";
                        addedItemsCounter++;
                    }
                }

                coupons.Clear();
            }

            BikeButton.Enabled = true;
            CouponsButton.Enabled = true;

            MessageBox.Show($"The items were successfully uploaded \n Total added items:{addedItemsCounter} ", "Success");
            progressBar1.Visible = false;
            importbtn.Enabled = true;
        }

        private async Task<Bike> StoreBikes(Bike bike)
        {
            HttpClient httpClient = new HttpClient();

            var json = JsonConvert.SerializeObject(bike);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Token", Token);
            var result = await httpClient.PostAsync(host + "bikes/", content);

            return result.StatusCode == System.Net.HttpStatusCode.Created ? JsonConvert.DeserializeObject<Bike>(await result.Content.ReadAsStringAsync()) : null;
        }

        private async Task<Sale> StoreSales(Sale sale)
        {
            HttpClient httpClient = new HttpClient();

            var json = JsonConvert.SerializeObject(sale);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            httpClient.DefaultRequestHeaders.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue("Token", Token);
            var result = await httpClient.PostAsync(host + "sales/", content);

            return result.StatusCode == System.Net.HttpStatusCode.Created ? JsonConvert.DeserializeObject<Sale>(await result.Content.ReadAsStringAsync()) : null;
        }

        private async Task<bool> AreCredentialsValidAsync(User newUser)
        {
            HttpClient httpClient = new HttpClient();
            string json = JsonConvert.SerializeObject(newUser);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await httpClient.PostAsync(host + "token-auth", content);
            if (result.IsSuccessStatusCode)
            {
                string jsonString = await result.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<TokenRespons>(jsonString);
                Token = token.Token;

                return true;
            }
            return false;
        }

        private async void loginbtn_Click(object sender, EventArgs e)
        {
            string _Username = usernametxt.Text;
            string _Password = passwordtxt.Text;
            User user = new User { username = _Username, password = _Password };
            if (await AreCredentialsValidAsync(user))
            {
                importbtn.Enabled = true;
                addsingelCouponPanel.Enabled = true;
                MessageBox.Show("Successfully logged in ", "Success");
            }
            else
            {
                MessageBox.Show("Error with login ", "Error");
            }
        }

        private async void AddCouponbtn_Click(object sender, EventArgs e)
        {
            string code = Couponcodetbx.Text;
            string value = CouponValuetbx.Text;
            string CouponAangebodenDoor = CouponAangebodenDoortbx.Text;
            string DatumUitgifte = DatumUitgiftetbx.Text;

            Coupon coupon = new Coupon
            {
                CouponCode = code,
                CouponValue = decimal.Parse(value),
                AangebodenDoor = CouponAangebodenDoor,
                Datum = DatumUitgifte

            };

            if (await StoreCoupon(coupon))
            {
                progressBar1.PerformStep();
                richTextBox1.Text += coupon.ToString() + "\n";
                MessageBox.Show("Coupon successfully added", "Success");
            }
            else
            {
                MessageBox.Show("Something went wrong...", "Error");
            }
        }

        private void ImportCouponsFromExcel()
        {
            progressBar1.Visible = true;
            coupons.Clear();
            richTextBox1.Clear();

            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Excel |*.xlsx";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = null;
                StreamReader sr = null;
                try
                {
                    fs = new FileStream(ofd.FileName, FileMode.Open, FileAccess.Read);
                    sr = new StreamReader(fs);

                    var filename = Path.GetFileName(ofd.FileName);
                    FileNamelbl.Text = filename;

                    Excel newfile = new Excel(ofd.FileName);

                    int rowCount = newfile.ListRange.Rows.Count;
                    int colCount = newfile.ListRange.Columns.Count;

                    progressBar1.Maximum = rowCount - 1;
                    progressBar1.Step = 1;
                    progressBar1.Value = 0;
                    for (int i = 2; i <= rowCount; i++)
                    {

                        //write the value to the console
                        if (newfile.ListRange.Cells[i, 1] != null && newfile.ListRange.Cells[i, 1].Value2 != null)
                            Cell1 = newfile.ListRange.Cells[i, 1].Value2.ToString(); //cell 1
                        else
                        {
                            Cell1 = "";
                        }


                        //write the value to the console
                        if (newfile.ListRange.Cells[i, 2] != null && newfile.ListRange.Cells[i, 2].Value2 != null)
                        {
                            var val = newfile.ListRange.Cells[i, 2].Value2.ToString();
                            var dateTime = double.Parse(val);
                            Cell2 = DateTime.FromOADate(dateTime).ToString("yyyy-MM-dd");

                        }
                        else
                        {
                            Cell2 = DateTime.Now.ToString("yyyy-MM-dd");
                        }

                        //write the value to the console
                        if (newfile.ListRange.Cells[i, 3] != null && newfile.ListRange.Cells[i, 3].Value2 != null)
                            Cell3 = newfile.ListRange.Cells[i, 3].Value2.ToString(); //cell 3
                        else
                        {
                            Cell3 = "";
                        }

                        Coupon coupon = new Coupon
                        {
                            CouponCode = Cell1,
                            Datum = Cell2,
                            AangebodenDoor = Cell3,
                            CouponValue = CouponVal,
                        };
                        coupons.Add(coupon);

                        progressBar1.PerformStep();
                        richTextBox1.Text += coupon.ToString() + "\n";
                    }

                    MessageBox.Show("The file was successfully imported", "Success");

                }
                catch (IOException) { MessageBox.Show("File is corrupted", "Error"); }
                catch (NullReferenceException ex) { MessageBox.Show(ex.Message, "Error"); }
                catch (Exception exs) { MessageBox.Show(exs.Message, "Error"); }

                finally
                {
                    if (sr != null) sr.Close();
                    if (fs != null) fs.Close();
                    progressBar1.Visible = false;
                    Uploadbtn.Enabled = true;
                    BikeButton.Enabled = true;
                    CouponsButton.Enabled = true;
                }
            }
        }

        private void ImportBikesFromExcel()
        {
            var culture = CultureInfo.CreateSpecificCulture("nl-NL");

            State ParseStatus(string text)
            {
                text = text.Trim().ToLower();

                if (String.Compare(text, "tekoop") == 0 || String.Compare(text, "te koop") == 0)
                    return State.ForSale;
                if (String.Compare(text, "verkocht") == 0)
                    return State.Sold;
                if (String.Compare(text, "reparatie") == 0 || String.Compare(text, "roc") == 0 ||
                                                              String.Compare(text, "r.o.c") == 0 ||
                                                              String.Compare(text, "r.o.c.") == 0)
                    return State.Repair;
                if (String.Compare(text, "sloop") == 0)
                    return State.Junk;

                if (String.Compare(text, "geretourneerd anwb") == 0)
                    return State.Removed;

                Console.WriteLine("Couldn't parse status: \'{0}\'", text);

                return State.Unknown;
            }

            Types ParseType(string text)
            {
                text = text.Trim().ToLower();

                if (String.Compare(text, "damesfiets") == 0 || String.Compare(text, "dames fiets") == 0)
                    return Types.Ladies;
                if (String.Compare(text, "omafiets") == 0 || String.Compare(text, "oma fiets") == 0)
                    return Types.Ladies;
                if (String.Compare(text, "moederfiets") == 0 || String.Compare(text, "moeder fiets") == 0)
                    return Types.Ladies;

                if (String.Compare(text, "herenfiets") == 0 || String.Compare(text, "heren fiets") == 0
                                                            || String.Compare(text, "heren") == 0)
                    return Types.Mens;
                if (String.Compare(text, "mountain bike") == 0 || String.Compare(text, "mountainbike") == 0
                                                               || String.Compare(text, "herenfiets /mtb") == 0)
                    return Types.Mountainbike;

                if (String.Compare(text, "wielrenfiets") == 0)
                    return Types.Road;

                if (String.Compare(text, "vouwfiets") == 0 || String.Compare(text, "vouw fiets") == 0)
                    return Types.Folding;

                if (String.Compare(text, "kinderfiets") == 0 || String.Compare(text, "kinder fiets") == 0
                                                             || String.Compare(text, "jongens") == 0
                                                             || String.Compare(text, "meisjes") == 0
                                                             || String.Compare(text, "kinderfiets meisje") == 0)
                    return Types.Kids;

                if (String.Compare(text, "crossfiets") == 0 || String.Compare(text, "bmx/kinderfiets") == 0)
                    return Types.BMX;

                Console.WriteLine("Couldn't parse state: \'{0}\'", text);

                return Types.Unknown;
            }

            double ParseNumber(string text)
            {
                char[] trim = {'?', '-' };

                if (Double.TryParse(text.Trim(trim), NumberStyles.Currency, culture, out double d))
                    return d;

                Console.WriteLine("Couldn't parse number: \'{0}\'", text);
                return Double.NaN;
            }

            progressBar1.Visible = true;
            bikes.Clear();
            richTextBox1.Clear();

            OpenFileDialog ofd = new OpenFileDialog
            {
                Filter = "Excel |*.xlsx"
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = null;
                StreamReader sr = null;

                try
                {
                    fs = new FileStream(ofd.FileName, FileMode.Open, FileAccess.Read);
                    sr = new StreamReader(fs);
                    var filename = Path.GetFileName(ofd.FileName);
                    FileNamelbl.Text = filename;

                    Excel newfile = new Excel(ofd.FileName);

                    int rowCount = newfile.ListRange.Rows.Count;
                    int colCount = newfile.ListRange.Columns.Count;

                    // MessageBox.Show($"imported {rowCount - 2} × {colCount} cells");

                    progressBar1.Maximum = rowCount - 1;
                    progressBar1.Step = 1;
                    progressBar1.Value = 0;

                    int bikesAdded = 0;

                    for (int i = 3; i <= rowCount; i++)
                    {
                        Bike bike = new Bike();

                        // Old bikenumber
                        if (newfile.ListRange.Cells[i, 1] != null && newfile.ListRange.Cells[i, 1].Value2 != null)
                            bike.OldId = Program.RemoveWhitespace(newfile.ListRange.Cells[i, 1].Value2.ToString()).ToLower(); //cell 2
                        else
                            bike.OldId = "unknown";

                        bike.Brand = null;
                        // Merk
                        if (newfile.ListRange.Cells[i, 2] != null && newfile.ListRange.Cells[i, 2].Value2 != null)
                            bike.Brand = newfile.ListRange.Cells[i, 2].Value2.ToString().Trim(); //cell 2

                        if (bike.Brand == null || bike.Brand == "-")
                            bike.Brand = "unknown";

                        bike.Model = null;
                        // write the value to the console
                        if (newfile.ListRange.Cells[i, 3] != null && newfile.ListRange.Cells[i, 3].Value2 != null)
                            bike.Model = newfile.ListRange.Cells[i, 3].Value2.ToString().Trim();

                        if (bike.Model == "-")
                            bike.Model = null;

                        // write the value to the console
                        if (newfile.ListRange.Cells[i, 4] != null && newfile.ListRange.Cells[i, 4].Value2 != null)
                            bike.Type = ParseType(newfile.ListRange.Cells[i, 4].Value2.ToString()).ToString(); //cell 4
                        else
                            bike.Type = Types.Unknown.ToString();

                        // write the value to the console
                        if (newfile.ListRange.Cells[i, 5] != null && newfile.ListRange.Cells[i, 5].Value2 != null)
                            bike.Color = newfile.ListRange.Cells[i, 5].Value2.ToString().Trim(); //cell 5
                        else
                            bike.Color = "unknown";

                        bike.FrameNumber = null;
                        //write the value to the console
                        if  (newfile.ListRange.Cells[i, 6] != null && newfile.ListRange.Cells[i, 6].Value2 != null)
                            bike.FrameNumber = newfile.ListRange.Cells[i, 6].Value2.ToString().Trim(); //cell 6

                        if (bike.FrameNumber == "-")
                            bike.FrameNumber = null;

                        // write the value to the console
                        if (newfile.ListRange.Cells[i, 7] != null && newfile.ListRange.Cells[i, 7].Value2 != null)
                            bike.State = ParseStatus(newfile.ListRange.Cells[i, 7].Value2.ToString()); //cell 7
                        else
                            bike.State = State.Repair;

                        // write the value to the console
                        if (newfile.ListRange.Cells[i, 8] != null && newfile.ListRange.Cells[i, 8].Value2 != null)
                        {
                            var x = newfile.ListRange.Cells[i, 8].Value2.ToString(); //cell 8

                            double d = ParseNumber(x);

                            bike.Price = null;

                            if (!double.IsNaN(d) && d > 0)
                                bike.Price = (decimal)d;
                        }

                        if (newfile.ListRange.Cells[i, 9] != null && newfile.ListRange.Cells[i, 9].Value2 != null)
                        {
                            var x = newfile.ListRange.Cells[i, 9].Value2.ToString(); //cell 9

                            if (double.TryParse(x, NumberStyles.Float, culture, out double height))
                                bike.FrameHeight = (int)height;
                            else
                            {
                                bike.FrameHeight = -1;
                                Console.WriteLine("Couldn't parse height: \'{0}\'", x);
                            }
                        }

                        bike.Source = Source.Unknown;
                        bike.AppearanceState = AppearanceState.Unknown;
                        bikes.Add(bike);

                        bikesAdded++;
                        progressBar1.PerformStep();
                        richTextBox1.Text += bike.ToString() + "\n";
                    }

                    rowCount = newfile.SoldRange.Rows.Count;

                    for (int i = 2; i <= rowCount; i++)
                    {
                        Bike bike = null;
                        Sale sale = null;
                        string SoldId = null;

                        try
                        {
                            bike = null; sale = null; SoldId = null;

                            if (newfile.SoldRange.Cells[i, 2] != null && newfile.SoldRange.Cells[i, 2].Value2 != null)
                                SoldId = Program.RemoveWhitespace(newfile.SoldRange.Cells[i, 2].Value2.ToString()).ToLower();

                            if (SoldId == null)
                                continue;

                            bike = bikes.Find(x => x.OldId == SoldId);
                            if (bike == null)
                            {
                                Console.WriteLine($"Couldn't locate bike {SoldId}...");
                                continue;
                            }

                            double price = double.NaN;

                            try
                            {
                                price = ParseNumber(newfile.SoldRange.Cells[i, 5].Value2.ToString());
                            }
                            catch
                            {
                            }

                            if (double.IsNaN(price))
                                price = 0;

                            DateTime timestamp = new DateTime(2017, 01, 01);

                            if (double.TryParse(newfile.SoldRange.Cells[i, 1].Value2.ToString(), out double d))
                                timestamp = DateTime.FromOADate(d);

                            bike.Sale = new Sale
                            {
                                BikeId = bike.Id,
                                FinalPrice = (decimal)price,
                                Sold_At = timestamp,
                            };

                            // Console.WriteLine($"Matched sale: bike #{bike.OldId}, sold at {bike.Sale.Sold_At} for {bike.Sale.FinalPrice}");
                        }
                        catch
                        {
                            if (bike != null && bike.Sale != null)
                                Console.WriteLine($"Fucking thing doesn't work: bike #{bike.OldId}, sold at {sale.Sold_At}");
                            if (bike != null)
                                Console.WriteLine($"Fucking thing doesn't work: bike #{bike.OldId}, sold at <unknown date>");
                            else if (SoldId != null)
                                Console.WriteLine($"Couldn't locate bike {SoldId}...");
                            else
                                Console.WriteLine("Couldn't match sale...");
                        }
                    }

                    MessageBox.Show($"The file was successfully imported, added {bikesAdded} bikes.", "Success");
                }
                catch (IOException) { MessageBox.Show("File is corrupted", "Error"); }
                catch (NullReferenceException ex) { MessageBox.Show(ex.Message, "Error"); }
                catch (Exception exs)
                {
                    MessageBox.Show(exs.Message, "Error");
                }
                finally
                {
                    if (sr != null) sr.Close();
                    if (fs != null) fs.Close();
                    progressBar1.Visible = false;
                    Uploadbtn.Enabled = true;
                    BikeButton.Enabled = true;
                    CouponsButton.Enabled = true;
                }
            }
        }


    }
}
