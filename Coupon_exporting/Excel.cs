﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using _Execl = Microsoft.Office.Interop.Excel;

namespace Data_exporting
{
    class Excel
    {
        /// <summary>
        /// Path to the excel file
        /// </summary>
        public string Path { get; set; }

        _Application excel;

        Workbook wb;
        Worksheet ws;
        Worksheet bs;

        public Excel(string Path)
        {
            this.Path = Path;

            excel = new _Execl.Application();
            wb = excel.Workbooks.Open(Path);

            ws = excel.Worksheets[1];
            bs = excel.Worksheets[2];
        }

        public Range ListRange { get { return ws.UsedRange; } }
        public Range SoldRange { get { return bs.UsedRange; } }
    }
}
