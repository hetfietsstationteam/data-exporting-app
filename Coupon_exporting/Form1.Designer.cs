﻿namespace Data_exporting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.loginbtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.passwordtxt = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.Label();
            this.usernametxt = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CouponsButton = new System.Windows.Forms.RadioButton();
            this.BikeButton = new System.Windows.Forms.RadioButton();
            this.Uploadbtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CouponValue = new System.Windows.Forms.TextBox();
            this.FileNamelbl = new System.Windows.Forms.Label();
            this.importbtn = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.addsingelCouponPanel = new System.Windows.Forms.Panel();
            this.AddCouponbtn = new System.Windows.Forms.Button();
            this.CouponValuelbl = new System.Windows.Forms.Label();
            this.CouponValuetbx = new System.Windows.Forms.TextBox();
            this.CouponAangebodenDoorlbl = new System.Windows.Forms.Label();
            this.CouponAangebodenDoortbx = new System.Windows.Forms.TextBox();
            this.DatumUitgiftelbl = new System.Windows.Forms.Label();
            this.DatumUitgiftetbx = new System.Windows.Forms.TextBox();
            this.Couponcodelbl = new System.Windows.Forms.Label();
            this.Couponcodetbx = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.addsingelCouponPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.loginbtn);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.passwordtxt);
            this.panel2.Controls.Add(this.username);
            this.panel2.Controls.Add(this.usernametxt);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(842, 87);
            this.panel2.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.pictureBox1);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(226, 82);
            this.panel8.TabIndex = 30;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(23, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.pictureBox1.Size = new System.Drawing.Size(203, 82);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(23, 82);
            this.panel9.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.progressBar1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 82);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(842, 5);
            this.panel5.TabIndex = 29;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point(0, 0);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(842, 5);
            this.progressBar1.TabIndex = 24;
            // 
            // loginbtn
            // 
            this.loginbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.loginbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginbtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginbtn.ForeColor = System.Drawing.Color.White;
            this.loginbtn.Location = new System.Drawing.Point(678, 23);
            this.loginbtn.Name = "loginbtn";
            this.loginbtn.Size = new System.Drawing.Size(136, 42);
            this.loginbtn.TabIndex = 28;
            this.loginbtn.Text = "Login";
            this.loginbtn.UseVisualStyleBackColor = false;
            this.loginbtn.Click += new System.EventHandler(this.loginbtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(458, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 23);
            this.label3.TabIndex = 16;
            this.label3.Text = "Password";
            // 
            // passwordtxt
            // 
            this.passwordtxt.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordtxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.passwordtxt.Location = new System.Drawing.Point(458, 35);
            this.passwordtxt.Name = "passwordtxt";
            this.passwordtxt.PasswordChar = '*';
            this.passwordtxt.Size = new System.Drawing.Size(205, 30);
            this.passwordtxt.TabIndex = 15;
            this.passwordtxt.Text = "123456";
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.username.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.ForeColor = System.Drawing.Color.White;
            this.username.Location = new System.Drawing.Point(240, 7);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(87, 23);
            this.username.TabIndex = 14;
            this.username.Text = "Username";
            // 
            // usernametxt
            // 
            this.usernametxt.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernametxt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.usernametxt.Location = new System.Drawing.Point(238, 36);
            this.usernametxt.Name = "usernametxt";
            this.usernametxt.Size = new System.Drawing.Size(205, 30);
            this.usernametxt.TabIndex = 13;
            this.usernametxt.Text = "yusra";
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(23, 74);
            this.panel7.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(128)))), ((int)(((byte)(140)))));
            this.panel1.Controls.Add(this.CouponsButton);
            this.panel1.Controls.Add(this.BikeButton);
            this.panel1.Controls.Add(this.Uploadbtn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.CouponValue);
            this.panel1.Controls.Add(this.FileNamelbl);
            this.panel1.Controls.Add(this.importbtn);
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 639);
            this.panel1.TabIndex = 0;
            // 
            // CouponsButton
            // 
            this.CouponsButton.AutoSize = true;
            this.CouponsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CouponsButton.ForeColor = System.Drawing.Color.White;
            this.CouponsButton.Location = new System.Drawing.Point(678, 110);
            this.CouponsButton.Name = "CouponsButton";
            this.CouponsButton.Size = new System.Drawing.Size(148, 24);
            this.CouponsButton.TabIndex = 29;
            this.CouponsButton.TabStop = true;
            this.CouponsButton.Text = "Import Coupons";
            this.CouponsButton.UseVisualStyleBackColor = true;
            // 
            // BikeButton
            // 
            this.BikeButton.AutoSize = true;
            this.BikeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BikeButton.ForeColor = System.Drawing.Color.White;
            this.BikeButton.Location = new System.Drawing.Point(551, 110);
            this.BikeButton.Name = "BikeButton";
            this.BikeButton.Size = new System.Drawing.Size(121, 24);
            this.BikeButton.TabIndex = 28;
            this.BikeButton.TabStop = true;
            this.BikeButton.Text = "Import bikes";
            this.BikeButton.UseVisualStyleBackColor = true;
            // 
            // Uploadbtn
            // 
            this.Uploadbtn.Enabled = false;
            this.Uploadbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Uploadbtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Uploadbtn.ForeColor = System.Drawing.Color.White;
            this.Uploadbtn.Location = new System.Drawing.Point(551, 316);
            this.Uploadbtn.Name = "Uploadbtn";
            this.Uploadbtn.Size = new System.Drawing.Size(263, 42);
            this.Uploadbtn.TabIndex = 27;
            this.Uploadbtn.Text = "Upload to DB";
            this.Uploadbtn.UseVisualStyleBackColor = true;
            this.Uploadbtn.Click += new System.EventHandler(this.Uploadbtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(550, 252);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 23);
            this.label1.TabIndex = 26;
            this.label1.Text = "Couponcode";
            // 
            // CouponValue
            // 
            this.CouponValue.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CouponValue.Location = new System.Drawing.Point(551, 280);
            this.CouponValue.Name = "CouponValue";
            this.CouponValue.Size = new System.Drawing.Size(263, 30);
            this.CouponValue.TabIndex = 25;
            this.CouponValue.Text = "20";
            this.CouponValue.TextChanged += new System.EventHandler(this.CouponValue_TextChanged);
            // 
            // FileNamelbl
            // 
            this.FileNamelbl.AutoSize = true;
            this.FileNamelbl.ForeColor = System.Drawing.Color.White;
            this.FileNamelbl.Location = new System.Drawing.Point(552, 153);
            this.FileNamelbl.Name = "FileNamelbl";
            this.FileNamelbl.Size = new System.Drawing.Size(0, 17);
            this.FileNamelbl.TabIndex = 24;
            // 
            // importbtn
            // 
            this.importbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.importbtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importbtn.ForeColor = System.Drawing.Color.White;
            this.importbtn.Location = new System.Drawing.Point(551, 173);
            this.importbtn.Name = "importbtn";
            this.importbtn.Size = new System.Drawing.Size(263, 42);
            this.importbtn.TabIndex = 22;
            this.importbtn.Text = "Import from Excel";
            this.importbtn.UseVisualStyleBackColor = true;
            this.importbtn.Click += new System.EventHandler(this.importbtn_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.richTextBox1.Location = new System.Drawing.Point(0, 87);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(517, 295);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.addsingelCouponPanel);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 382);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(842, 257);
            this.panel3.TabIndex = 1;
            // 
            // addsingelCouponPanel
            // 
            this.addsingelCouponPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.addsingelCouponPanel.Controls.Add(this.AddCouponbtn);
            this.addsingelCouponPanel.Controls.Add(this.CouponValuelbl);
            this.addsingelCouponPanel.Controls.Add(this.CouponValuetbx);
            this.addsingelCouponPanel.Controls.Add(this.CouponAangebodenDoorlbl);
            this.addsingelCouponPanel.Controls.Add(this.CouponAangebodenDoortbx);
            this.addsingelCouponPanel.Controls.Add(this.DatumUitgiftelbl);
            this.addsingelCouponPanel.Controls.Add(this.DatumUitgiftetbx);
            this.addsingelCouponPanel.Controls.Add(this.Couponcodelbl);
            this.addsingelCouponPanel.Controls.Add(this.Couponcodetbx);
            this.addsingelCouponPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addsingelCouponPanel.Location = new System.Drawing.Point(150, 0);
            this.addsingelCouponPanel.Name = "addsingelCouponPanel";
            this.addsingelCouponPanel.Size = new System.Drawing.Size(542, 257);
            this.addsingelCouponPanel.TabIndex = 14;
            // 
            // AddCouponbtn
            // 
            this.AddCouponbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddCouponbtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddCouponbtn.ForeColor = System.Drawing.Color.White;
            this.AddCouponbtn.Location = new System.Drawing.Point(288, 126);
            this.AddCouponbtn.Name = "AddCouponbtn";
            this.AddCouponbtn.Size = new System.Drawing.Size(228, 42);
            this.AddCouponbtn.TabIndex = 21;
            this.AddCouponbtn.Text = "Add Coupon";
            this.AddCouponbtn.UseVisualStyleBackColor = true;
            this.AddCouponbtn.Click += new System.EventHandler(this.AddCouponbtn_Click);
            // 
            // CouponValuelbl
            // 
            this.CouponValuelbl.AutoSize = true;
            this.CouponValuelbl.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CouponValuelbl.ForeColor = System.Drawing.Color.White;
            this.CouponValuelbl.Location = new System.Drawing.Point(284, 29);
            this.CouponValuelbl.Name = "CouponValuelbl";
            this.CouponValuelbl.Size = new System.Drawing.Size(118, 23);
            this.CouponValuelbl.TabIndex = 18;
            this.CouponValuelbl.Text = "Coupon Value";
            // 
            // CouponValuetbx
            // 
            this.CouponValuetbx.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CouponValuetbx.Location = new System.Drawing.Point(288, 57);
            this.CouponValuetbx.Name = "CouponValuetbx";
            this.CouponValuetbx.Size = new System.Drawing.Size(228, 30);
            this.CouponValuetbx.TabIndex = 17;
            // 
            // CouponAangebodenDoorlbl
            // 
            this.CouponAangebodenDoorlbl.AutoSize = true;
            this.CouponAangebodenDoorlbl.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CouponAangebodenDoorlbl.ForeColor = System.Drawing.Color.White;
            this.CouponAangebodenDoorlbl.Location = new System.Drawing.Point(29, 170);
            this.CouponAangebodenDoorlbl.Name = "CouponAangebodenDoorlbl";
            this.CouponAangebodenDoorlbl.Size = new System.Drawing.Size(154, 23);
            this.CouponAangebodenDoorlbl.TabIndex = 16;
            this.CouponAangebodenDoorlbl.Text = "Aangeboden door ";
            // 
            // CouponAangebodenDoortbx
            // 
            this.CouponAangebodenDoortbx.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CouponAangebodenDoortbx.Location = new System.Drawing.Point(33, 196);
            this.CouponAangebodenDoortbx.Name = "CouponAangebodenDoortbx";
            this.CouponAangebodenDoortbx.Size = new System.Drawing.Size(228, 30);
            this.CouponAangebodenDoortbx.TabIndex = 15;
            // 
            // DatumUitgiftelbl
            // 
            this.DatumUitgiftelbl.AutoSize = true;
            this.DatumUitgiftelbl.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatumUitgiftelbl.ForeColor = System.Drawing.Color.White;
            this.DatumUitgiftelbl.Location = new System.Drawing.Point(29, 100);
            this.DatumUitgiftelbl.Name = "DatumUitgiftelbl";
            this.DatumUitgiftelbl.Size = new System.Drawing.Size(232, 23);
            this.DatumUitgiftelbl.TabIndex = 14;
            this.DatumUitgiftelbl.Text = "Datum uitgifte (yyyy-MM-dd)";
            // 
            // DatumUitgiftetbx
            // 
            this.DatumUitgiftetbx.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatumUitgiftetbx.Location = new System.Drawing.Point(33, 126);
            this.DatumUitgiftetbx.Name = "DatumUitgiftetbx";
            this.DatumUitgiftetbx.Size = new System.Drawing.Size(228, 30);
            this.DatumUitgiftetbx.TabIndex = 13;
            // 
            // Couponcodelbl
            // 
            this.Couponcodelbl.AutoSize = true;
            this.Couponcodelbl.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Couponcodelbl.ForeColor = System.Drawing.Color.White;
            this.Couponcodelbl.Location = new System.Drawing.Point(29, 29);
            this.Couponcodelbl.Name = "Couponcodelbl";
            this.Couponcodelbl.Size = new System.Drawing.Size(108, 23);
            this.Couponcodelbl.TabIndex = 12;
            this.Couponcodelbl.Text = "Couponcode";
            // 
            // Couponcodetbx
            // 
            this.Couponcodetbx.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Couponcodetbx.Location = new System.Drawing.Point(33, 57);
            this.Couponcodetbx.Name = "Couponcodetbx";
            this.Couponcodetbx.Size = new System.Drawing.Size(228, 30);
            this.Couponcodetbx.TabIndex = 11;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(150, 257);
            this.panel6.TabIndex = 13;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(160)))), ((int)(((byte)(174)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(692, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 257);
            this.panel4.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(842, 639);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Data exporting app";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.addsingelCouponPanel.ResumeLayout(false);
            this.addsingelCouponPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label username;
        private System.Windows.Forms.TextBox usernametxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox passwordtxt;
        private System.Windows.Forms.Button loginbtn;
        private System.Windows.Forms.Label FileNamelbl;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel addsingelCouponPanel;
        private System.Windows.Forms.Button AddCouponbtn;
        private System.Windows.Forms.Label CouponValuelbl;
        private System.Windows.Forms.TextBox CouponValuetbx;
        private System.Windows.Forms.Label CouponAangebodenDoorlbl;
        private System.Windows.Forms.TextBox CouponAangebodenDoortbx;
        private System.Windows.Forms.Label DatumUitgiftelbl;
        private System.Windows.Forms.TextBox DatumUitgiftetbx;
        private System.Windows.Forms.Label Couponcodelbl;
        private System.Windows.Forms.TextBox Couponcodetbx;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button Uploadbtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CouponValue;
        private System.Windows.Forms.Button importbtn;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RadioButton CouponsButton;
        private System.Windows.Forms.RadioButton BikeButton;
    }
}

