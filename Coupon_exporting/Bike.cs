﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;


namespace Data_exporting
{
	public enum State
	{
		[Display(Description = "Reparatie")]
		Repair = 1,
		[Display(Description = "Sloop")]
		Junk = 2,
		[Display(Description = "Te koop")]
		ForSale = 3,
		[Display(Description = "R.O.C.")]
		School = 4,
		[Display(Description = "Verkocht")]
		Sold = 5,
		[Display(Description = "verwijderd")]
		Removed = 6,

		[Display(Description = "Onbekend")]
		Unknown = 0,
	}

	public enum Source
	{
		[Display(Description = "Anders")]
		Other = 1,
		[Display(Description = "NS")]
		NS = 2,
		[Display(Description = "ANWB")]
		ANWB = 3,

		[Display(Description = "Onbekend")]
		Unknown = 0,
	}

	public enum AppearanceState
	{
		[Display(Description = "Slecht")]
		Bad = 1,
		[Display(Description = "Redelijk")]
		Acceptable = 5,
		[Display(Description = "Goed")]
		Excellent = 9,

		[Display(Description = "Onbekend")]
		Unknown = 0,
	}

	public enum Types
	{
		[Display(Description = "Herenfiets")]
		Mens = 1,
		[Display(Description = "Damesfiets")]
		Ladies,
		[Display(Description = "Kinderfiets")]
		Kids,
		[Display(Description = "E-bike")]
		EBike,
		[Display(Description = "Mountainbike")]
		Mountainbike,
		[Display(Description = "Renfiets / tourfiets")]
		Road,
		[Display(Description = "Vouwfiets")]
		Folding,
		[Display(Description = "BMX / cross")]
		BMX,

		[Display(Description = "Anders")]
		Unknown = 0,
	}

	[JsonObject(MemberSerialization.OptIn)]
	public class Bike
	{
		// properties from REST

		public string OldId { get; set; }
		private Sale sale = null;
		public Sale Sale { get { return sale; } set { sale = value; } }
		//public DateTime SoldDate { get; set; }


		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("brand")]
		public string Brand { get; set; }
		[JsonProperty("model")]
		public string Model { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("frame_number")]
		public string FrameNumber { get; set; }
		[JsonProperty("frame_height")]
		public int FrameHeight { get; set; }
		[JsonProperty("color")]
		public string Color { get; set; }

		[JsonProperty("appearance")]
		public AppearanceState AppearanceState { get; set; }

		[JsonProperty("source")]
		public Source Source { get; set; }
		[JsonProperty("status")]
		public State State { get; set; }
		[JsonProperty("price")]
		public decimal? Price { get; set; }

		[JsonProperty("mechanic")]
		public string BikeMechanic { get; set; }

		[JsonProperty("notes")]
		public string Note { get; set; }

		[JsonProperty("registered")]
		public DateTime RegisterDate { get; set; }
		[JsonProperty("repaired")]
		public DateTime? DateMarkedAsReady { get; set; }

		[JsonProperty("image")]
		public BikeImageArr Image { get; set; }

		[JsonProperty("flags")]
		public List<string> Flags { get; set; }

		// JSON conversion helpers

		[OnSerializing]
		internal void OnSerializingMethod(StreamingContext context)
		{
			if (Flags == null)
				Flags = new List<string>();
		}

		// C# properties/methods

		public override string ToString()
		{
			return $"{Brand}, {Model} #{Id}";
		}
	}

	[JsonObject(MemberSerialization.OptIn)]
	public partial class BikeImageArr
	{
		[JsonProperty("full-size")]
		public Uri FullSize { get; set; }
	}

	/* public class Bike
    {
        private decimal? price;

        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Type { get; set; }
        public string FrameNumber { get; set; }
        public int FrameHeight { get; set; }
        public string Color { get; set; }
        public string State { get; set; }
        public string AppearanceState { get; set; }
        public decimal? Price
        {
            get { return price; }
            set { price = value; }
        }
        public string BikeMechanic { get; set; }
        public DateTime? DateMarkedAsReady { get; set; }
        public string Note { get; set; }
        public DateTime RegisterDate { get; set; }

        public override string ToString()
        {
            return "Mark:" + Brand +", Model:" + Model + ", State:" + State;
        }
    } */
}
