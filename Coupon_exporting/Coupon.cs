﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_exporting
{
    public class Coupon
    {
        public int id { get; set; }
        public string Datum { get; set; }
        public string CouponCode { get; set; }
        public decimal CouponValue { get; set; }
        public string AangebodenDoor { get; set; }
        public DateTime Created_At { get; set; }


        public override string ToString()
        {
            return "Coupon Code: " + CouponCode + " Coupon Value: "+
                CouponValue +" AangebodenDoor: " + AangebodenDoor + " Datum: " + Datum;
        }
    }
}
